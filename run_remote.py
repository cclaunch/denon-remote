#!/usr/bin/env python

import time
import logging
from denon_remote import DenonRemote

def main():
    # TODO argparse
    remote = DenonRemote('COM3', log_level=logging.DEBUG)
    remote.open()
    # remote.set_volume('50')
    remote.mute_on()
    # remote.power_status()
    remote.start_listening()
    time.sleep(5)
    remote.mute_off()
    time.sleep(5)
    remote.mute_on()
    # remote.select_input('dvd1')
    # remote.listen()
    # remote.power_standby()
    # remote.power_on()
    # remote.set_source('tv')
    # remote.toggle_power()
    remote.close()

if __name__ == '__main__':
    main()
