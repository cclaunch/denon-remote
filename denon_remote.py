import binascii
import serial
import time
import logging
import threading

class DenonRemote(threading.Thread):
    """Class summary here

    More class info
    More class info

    Attributes:
        port_name: A string name of serial port to use for communicating with 
            the AVR.  Example: 'COM3'
        baud_rate: An integer specifying the baud rate of the serial port.
            Defaults to 9600.
        logger: A standard Python logging class instance.
    """
    def __init__(self, port_name, log_level=logging.INFO, baud_rate=9600):
        """Initializes DenonRemote with the given settings.
        """
        threading.Thread.__init__(self)
        self.port_name = port_name
        self.baud_rate = baud_rate
        self.inputs = ['phono', 'cd', 'tuner', 'dvd', 'tv', 'vcr-1', 'vcr-2', 'v.aux', 'cdr/tape']
        self.command_list = []
        self.init_logging(log_level)

    def open(self):
        """
        """
        self.logger.debug('opening %s', self.port_name)
        self.port = serial.Serial(self.port_name, self.baud_rate, 8, 
            serial.PARITY_NONE, serial.STOPBITS_ONE, xonxoff=0, rtscts=0, 
            timeout=1)

    def close(self):
        """
        """
        self.logger.debug('closing %s', self.port_name)
        self.port.close()      

    def start_listening(self):
        self.listen_thread = threading.Thread(target=self.listen)
        self.listen_thread.start()

    def listen(self):
        while self.port.isOpen():
            try:
                if self.port.inWaiting() > 0:
                    resp = self.port.readline()
                    resps = resp.splitlines()
                    if len(resps) <= 0:
                        self.logger.debug('received %s', resp)
                    else:
                        self.logger.debug('received %s', resps)
                    time.sleep(0.1)
            except:
                pass

    def power_status(self):
        self.send_receive('PW?')

    def power_standby(self):
        self.send_receive('PWSTANDBY')
    
    def power_on(self):
        self.send_receive('PWON')

    def mute_on(self):
        self.send_receive('MUON')

    def mute_off(self):
        self.send_receive('MUOFF')
    
    def select_input(self, input):
        if input in self.inputs:
            self.send_receive('SI' + input.upper())
        else:
            self.logger.error('invalid input: %s. valid inputs are: %s', input, self.inputs)

    def set_volume(self, volume):
        """
        '80' = max (0dB)
        '00' = min
        '99' = --- (off)s
        """
        self.send_receive('MV', volume)  

    # def toggle_power(self):
    #     """
    #     """
    #     self.logger.info('toggling power')
    #     self.send_receive(0x08, 0x22, 0x00, 0x00, 0x00, 0)

    # def set_volume(self, volume_level):
    #     """
    #     """
    #     if volume_level > 100:
    #         self.logger.error('volume requested out of range: %d', volume_level)
    #         return

    #     self.logger.info('setting volume to %d', volume_level)
    #     self.send_receive(0x08, 0x22, 0x01, 0x00, 0x00, volume_level)

    # def set_source(self, source_name):
    #     """
    #     """
    #     source = -1
    #     if source_name is 'tv':
    #         source = 0
    #     elif source_name is 'av1':
    #         source = 1
    #     elif source_name is 'av2':
    #         source = 2
    #     elif source_name is 'svideo1':
    #         source = 3
    #     elif source_name is 'svideo2':
    #         source = 4
    #     elif source_name is 'comp1':
    #         source = 5
    #     elif source_name is 'comp2':
    #         source = 6
    #     elif source_name is 'pc':
    #         source = 7
    #     elif source_name is 'hdmi1':
    #         source = 8
    #     elif source_name is 'hdmi2':
    #         source = 9
    #     elif source_name is 'av3':
    #         source = 10      
    #     else:
    #         self.logger.error('invalid source name %s', source_name)
    #         return

    #     self.logger.info('setting source to %s', source_name)
    #     self.send_receive(0x08, 0x22, 0x0a, 0x00, 0x00, source)
    def send_receive(self, cmd, param=''):
        """
        """
        if self.port.isOpen() == False:
            self.logger.error('send_receive failed, port not open')
            return

        if not param:
            command = cmd + '\r'
        else:
            command = cmd + param + '\r'

        hexstr = str(binascii.hexlify(command))
        formatted_hex = ' '.join(hexstr[i:i+2] for i in range(0, len(hexstr), 2))

        self.port.write(bytearray(command))
        self.logger.debug('sent %s', command.rstrip())

        # resp = self.port.readline()
        # resps = resp.splitlines()
        # if len(resps) <= 0:
        #     self.logger.debug('received %s', resp)
        # else:
        #     self.logger.debug('received %s', resps)

        # if resp == b'\x03\x0c\xf1':
        #     self.logger.info('command valid')
        #     return True
        # else:
        #     self.logger.error('command failed')
        #     return False

    # def send_receive(self, hdr1, hdr2, cmd1, cmd2, cmd3, val):
    #     """
    #     """
    #     if self.port.isOpen() == False:
    #         self.logger.error('Tried to send a command but port wasn\'t open')
    #         return

    #     command = bytearray([hdr1, hdr2, cmd1, cmd2, cmd3, val])

    #     checksum = -(sum(command) % 256) & 0xff
    #     command.append(checksum)

    #     hexstr = str(binascii.hexlify(command))
    #     formatted_hex = ' '.join(hexstr[i:i+2] for i in range(0, len(hexstr), 2))

    #     self.port.write(command)        
    #     self.logger.debug('sent %s', formatted_hex)

    #     resp = self.port.readline()
    #     formatted_resp = ' '.join("{:02x}".format(ord(c)) for c in resp)
    #     self.logger.debug('received %s', formatted_resp)

    #     if resp == b'\x03\x0c\xf1':
    #         self.logger.info('command valid')
    #         return True
    #     else:
    #         self.logger.error('command failed')
    #         return False

    def init_logging(self, log_level):
        """Initialize logging for the remote

        Args:
            log_level: A enum which can be DEBUG, INFO, WARNING, ERROR,
                or CRITICAL.  Example: logging.INFO
        """
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(log_level)
        formatter = logging.Formatter('%(asctime)s %(name)s [%(levelname)s] '
            '%(message)s')
        ch = logging.StreamHandler()
        ch.setLevel(log_level)
        ch.setFormatter(formatter)
        self.logger.addHandler(ch)
        self.logger.debug('logging initialized')
